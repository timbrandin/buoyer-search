LocalCache = new Meteor.Collection(null);

Router.map(function() {
  this.route('home', {
    path: '/',
    template: 'app'
  });

  this.route('image', {
    path: '/i/:_id',
    waitOn: function() {
			return Meteor.subscribe('buoyies', this.params._id);
		},
    onBeforeAction: function() {
      Meteor.call('elasticSearch', "images", this.params._id, function(e, r) {
        if (!e) {
          var result = JSON.parse(r[1]);
          if (result.hits) {
            for(var i in result.hits.hits) {
              LocalCache.insert(result.hits.hits[i]);
            }
          }
          else {
            console.log('something wrong');
          }
        }
      });
    },
    data: function() {

      var buoyie = Buoyies.findOne(this.params._id);

      return {
        image: buoyie,
        result: LocalCache.find()
      }
    }
  });
});
