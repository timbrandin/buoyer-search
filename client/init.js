Meteor.startup(function () {
	$(window).resize(function() {
		var bodyheight = $(window).height();
		$(".main").height(bodyheight);
		$('#home-input')
			.css('margin-top',
					 bodyheight/2
					);
		$('#home-input .center-container')
			.css('margin-top',
					 -$('#home-input .center-container').height()/2
					);
	});
});

Meteor.methods({
  s3done:function(url, context){
    console.log('done', url);
  }
});
