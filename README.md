# Buoyer

## How to get started

The frontend application for Buoyer buying and selling platform.

### Getting started

1. Install Meteor

    `curl -O https://install.meteor.com/ | sh`

2. Clone the repository

    `git clone git@bitbucket.org:timbrandin/buoyer.git`

    `cd buoyer`

3. Update Meteorite packages

    `mrt update`

4. Run Meteor

    `meteor`

5. Visit the browser at http://localhost:3000
