var Future = Npm.require("fibers/future");

ElasticImage = (function () {
    var ElasticSearchClient,
        Searchers,
        indexes = {},
        config = {
            host: '192.168.10.118',
            port: 9200,
            secure: false
        },
        conditions = {
            'onChangeProperty' : function () {
                return true;
            }
        },
        defaultQuery = function (searchFields, searchString) {
            return {
                "fuzzy_like_this" : {
                    "fields" : searchFields,
                    "like_text" : searchString
                }
            };
        },
        Future = Npm.require('fibers/future'),
        ElasticSearchInstance = Npm.require('elasticsearchclient');

    /**
     * Return Elastic Search indexable data.
     *
     * @param {Object} doc
     * @returns {Object}
     */
    function getESFields(doc) {
        var newDoc = {};

        _.each(doc, function (value, key) {
            newDoc[key] = "string" === typeof value ? value : JSON.stringify(value);
        });

        return newDoc;
    }

    /**
     * Write a document to a specified index.
     *
     * @param {String} name
     * @param {Object} doc
     * @param {String} id
     */
    function writeToIndex(name, doc, id) {
        // add to index
        ElasticSearchClient.index(name, 'image', doc, id)
            .on('data', function (data) {
                if (config.debug && console) {
                    console.log('EasySearch: Added / Replaced document to Elastic Search:');
                    console.log('EasySearch: ' + data + "\n");
                }
            })
            .exec();
    }

    /**
     * Searchers which contain all types which can be used to search content, until now:
     *
     * elastic-search: Use an elastic search server to search with (fast)
     * mongo-db: Use mongodb to search (more convenient)
     */
    Searchers = {
        'elastic-search' : {
            /**
             * Setup some observers on the mongo db collection provided.
             *
             * @param {String} name
             * @param {Object} options
             */
            'createSearchIndex' : function (name, options) {
                if ("undefined" === typeof ElasticSearchClient) {
                    ElasticSearchClient = new ElasticSearchInstance(config)
                }
            },
            /**
             * Get a fake representation of a mongo document.
             *
             * @param {Object} data
             * @returns {Array}
             */
            'getMongoDocumentObject' : function (data) {
                data = _.isString(data) ? JSON.parse(data) : data;

                return _.map(data.hits.hits, function (resultSet) {
                    var mongoDbDocFake = resultSet['_source'];

                    mongoDbDocFake['_id'] = resultSet['_id'];

                    return resultSet['_source'];
                });
            },
            /**
             * Perform a search with Elastic Search, using fibers.
             *
             * @param {String} name
             * @param {String} searchString
             * @param {String|Array} fields
             * @param {Function} callback
             * @returns {*}
             */
            'search' : function (name, id, fields, callback) {
                var queryObj,
                    that = this,
                    searchFields,
                    fut = new Future(),
                    index = indexes[name];

                if ("function" === typeof fields) {
                    callback = fields;
                    fields = [];
                }

                if (!_.isObject(index)) {
                    return;
                }

                searchFields = _.isArray(index.field) ? index.field : [index.field];
                queryObj = {
                    // "query" : index.query(searchFields, id),
                    "query": {
                      "image": {
                        "my_img": {
                          "feature": "CEDD",
                          "index": "buoyer",
                          "type": "image",
                          "id": id,
                          "path": "my_img",
                          "hash": "BIT_SAMPLING"
                        }
                      }
                    },
                    "fields": ["_id", "url"],
                    "size" : index.limit
                };

                if ("function" === typeof callback) {
                    ElasticSearchClient.search("buoyer", "image", queryObj, callback);
                    return;
                }

                // Most likely client call, return data set
                ElasticSearchClient.search(name, 'default_type', queryObj, function (error, data) {
                    if ("mongo" === index.format) {
                        data = that.getMongoDocumentObject(data);
                    }

                    if (_.isArray(fields) && fields.length > 0) {
                        data = _.map(data, function (doc) {
                            var i,
                                newDoc = {};

                            for (i = 0; i < fields.length; i += 1) {
                                newDoc[fields[i]] = doc[fields[i]];
                            }

                            return newDoc;
                        });
                    }

                    fut['return'](data);
                });

                return fut.wait();
            }
        }
    };

    return {
        /**
         * Override the config for Elastic Search.
         *
         * @param {object} newConfig
         */
        'config' : function (newConfig) {
            if ("undefined" === typeof newConfig) {
                return config;
            }

            config = _.extend(config, newConfig);
            ElasticSearchClient = new ElasticSearchInstance(config);
        },
        /**
         * Override conditions or return conditions if no parameter passed.
         *
         * @param newConditions
         * @returns {object}
         */
        'conditions' : function (newConditions) {
            if ("undefined" === typeof newConditions) {
                return conditions;
            }

            conditions = _.extend(conditions, newConditions);
        },
        /**
         * Create a search index for Elastic Search, which resembles a MongoDB Collection.
         *
         * @param {String} name
         * @param {Object} options
         */
        'setupSearchIndex' : function (name, options) {
            options.format = "string" === typeof options.format ? options.format : "mongo";
            options.limit = "number" === typeof options.limit ? options.limit : 10;
            options.query = "function" === typeof options.query ? options.query : defaultQuery;
            options.use = "string" === typeof options.use ? options.use : 'elastic-search';

            indexes[name] = options;

            if ("undefined" === typeof Searchers[options.use]) {
                throw new Meteor.Error(500, "Didnt find the type: '" + options.use + "' to be searched with.");
            }

            Searchers[options.use].createSearchIndex(name, options);
        },
        /**
         * Get a fake representation of a mongo document.
         *
         * @param {Object} data
         * @returns {Array}
         */
        'getMongoDocumentObject' : function (data) {
            data = _.isString(data) ? JSON.parse(data) : data;

            return _.map(data.hits.hits, function (resultSet) {
                var mongoDbDocFake = resultSet['_source'];

                mongoDbDocFake['_id'] = resultSet['_id'];

                return resultSet['_source'];
            });
        },
        /**
         * Perform a search.
         *
         * @param {String} name
         * @param {String} searchString
         * @param {Array} fields
         * @param {Function} callback
         */
        'search' : function (name, id, callback) {
          var searcherType = indexes[name].use;

          if ("undefined" === typeof Searchers[searcherType]) {
              throw new Meteor.Error(500, "Couldnt search with the type: '" + searcherType + "'");
          }

          var fields = "_id";

          return Searchers[searcherType].search(name, id, fields, callback);
        },
        /**
         * Change a property specified for the index.
         *
         * @param {String} name
         * @param {String} key
         * @param {String} value
         */
        'changeProperty' : function(name, key, value) {
            if (!_.isString(name) || !_.isString(key)) {
                throw new Meteor.Error('name and key of the property have to be strings!');
            }

            indexes[name][key] = value;
        },
        /**
         * Get the ElasticSearchClient
         * @see https://github.com/phillro/node-elasticsearch-client
         *
         * @return {ElasticSearchInstance}
         */
        'getElasticSearchClient' : function () {
            return ElasticSearchClient;
        },
        /**
          * Retrieve all index configurations
          */
        'getIndexes' : function () {
            return indexes;
        },
        /**
         * Perform a search with Elastic Search, using fibers.
         *
         * @param {String} name
         * @param {String} searchString
         * @param {String|Array} fields
         * @param {Function} callback
         * @returns {*}
         */
        'writeToIndex' : function (name, doc, id) {
          writeToIndex(name, doc, id);
        },
    };
})();

Meteor.methods({
    /**
     * Make search possible on the client.
     *
     * @param {String} name
     * @param {String} searchString
     */
    elasticSearch: function (name, id) {
      var fut = new Future();
      ElasticImage.search(name, id, function() {
        fut.return(arguments);
      });
      return fut.wait();
    },
    /**
     * Make indexing possible from the client.
     *
     * @param {String} name
     * @param {String} searchString
     */
    elasticIndex: function (name, doc, id) {
      return ElasticImage.writeToIndex(name, doc, id);
    },
    /**
     * Make changing properties possible on the client.
     *
     * @param {String} name
     * @param {String} key
     * @param {String} value
     */
    elasticSearchChangeProperty: function(name, key, value) {
        if (ElasticImage.conditions().onChangeProperty(name, key, value)) {
            ElasticImage.changeProperty(name, key, value);
        }
    }
});
