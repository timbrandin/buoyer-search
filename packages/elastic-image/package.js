Package.describe({
  summary: "REPLACEME - What does this package (or the original one you're wrapping) do?"
});

Npm.depends({
  'elasticsearchclient': '0.5.3'
});

Package.on_use(function (api, where) {
  api.add_files('elastic-image.js', 'server');

  api.export('ElasticImage');
});

Package.on_test(function (api) {
  api.use('elastic-image');

  api.add_files('elastic-image_tests.js', ['client', 'server']);
});
