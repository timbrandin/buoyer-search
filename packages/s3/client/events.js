Template.S3.events({
	'change input[type=file]': function (e,helper) {
		var context = this || {};

		if(helper.data && _.has(helper.data,"done")){
			var init = helper.data.init;
			var done = helper.data.done;
			var ready = helper.data.ready;
		} else {
			console.log("S3 Error: Helper Block needs a callback function to run");
			return
		}

		var files = e.currentTarget.files;
		_.each(files,function(file){
			var reader = new FileReader;
			var fileData = {
				name:file.name,
				size:file.size,
				type:file.type
			};

			reader.onload = function () {
				fileData.data = new Uint8Array(reader.result);
				Meteor.call("S3upload",fileData,context,init,done,function (e,imgId) {
					// Hack!
					console.log("NAV!");
					Router.go("/i/"+imgId);
				});
			};

			reader.readAsArrayBuffer(file);

		});
	}
});
